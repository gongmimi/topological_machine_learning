from Topological_ML import tda_function as tdap
from sklearn.datasets import fetch_california_housing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import kmapper as km
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn import ensemble

def test_data_summary():
    data = pd.DataFrame({"A": [1,2,3,4,5,6,7,8,9,10]})
    correct_response = {"head": [1,2,3,4,5,6,7,8,9,10], "shape": [10, 1], "describe": 0}
    values = tdap.data_summary(data, 5)
    assert values == correct_response
    return

def test_linear_regression():
    a = pd.DataFrame({"A": [1,2,3,4,5,6,7,8,9,10]})
    b = pd.DataFrame({"B": [2,4,6,8,10,12,14,16,18,20]})
    correct_response = 1
    values = tdap.linear_regression(a, b)
    assert values == correct_response
    return

def test_lens_1d():
    a = pd.DataFrame({"A": [0,0]})
    correct_response = np.array([[0., 0.],[0., 0.]])
    values = tdap.lens_1d(a,123,1)
    assert values == correct_response
    return

